/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Query;
import org.hibernate.Session;



/**
 *
 * @author clayderman soares
 */
public class UsuarioDAO {

	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("factory");
	
	private EntityManager em = factory.createEntityManager();
	
	
	
    public void inserir(Usuario a) {
        
    	em.getTransaction().begin();
    	em.persist(a);
    	em.getTransaction().commit();
        em.close();
        factory.close();
    }

    public Usuario pesquisaUsuario(int a) {
        Usuario Usuario = new Usuario();
        Usuario.setIdUsuario(a);
        em.getTransaction().begin();
        Usuario = (Usuario) em.createQuery("SELECT p from Usuario p where p.id = :id")
				  .setParameter("id", a).getSingleResult();
        return Usuario;
    }
    
    public Usuario pesquisaUsuarioId(int a) {
        Usuario usuario = new Usuario();
        usuario.setIdUsuario(a);
        usuario = (Usuario) em.createQuery("SELECT p from Usuario p where p.id = :id")
				  .setParameter("id", a).getSingleResult();
        return usuario;
    }
    

    @SuppressWarnings("unchecked")
	public Collection<Usuario> listar() {
        Collection<Usuario> Usuarios = null;
        em.getTransaction().begin();
        Usuarios = em.createQuery("select a from Usuario a").getResultList();
        em.getTransaction().commit();
        em.close();
        return Usuarios;
    }

    public void excluir(int id) {
        
    	em.getTransaction().begin();
        Usuario a = new Usuario();
        a.setIdUsuario(id);
        Usuario ab = pesquisaUsuarioId(a.getIdUsuario());
        em.remove(ab);
        em.getTransaction().commit();
        em.close();
        factory.close();
    }

    public void editar(Usuario a) {
        
    	em.getTransaction().begin();
    	em.merge(a);
        em.getTransaction().commit();
        em.close();
        factory.close();
    }

}
