/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Telefone.Telefone;
import Telefone.TelefoneDAO;

/**
 *
 * @author PC GAMER
 */
public class UsuarioControlador extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
            String acao = request.getParameter("acao");
            RequestDispatcher dispatcher;
            if (acao.equals("inserir")) {
                this.inserir(request, response);
                String url = "UsuarioControlador?acao=listar";
                response.sendRedirect(url);

            } else if (acao.equals("excluir")) {
                this.excluir(request, response);
                String url = "menu-gestor.jsp";
                response.sendRedirect(url);

            } else if (acao.equals("iniciarEditar")) {
                this.pesquisar(request, response);
                String url = "cad-usuario.jsp";
                dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);

            } else if (acao.equals("novoCadastro")) {
                TelefoneDAO td = new TelefoneDAO();
                Collection<Telefone> lista = td.listar();
                request.setAttribute("turmas", lista);
                request.setAttribute("acao", "carregaCadastro");
                String url = "cad-usuario.jsp";
                dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);

            } else if (acao.equals("listar")) {
                this.listar(request, response);
                String url = "lista-usuarios.jsp";
                dispatcher = request.getRequestDispatcher(url);
                dispatcher.forward(request, response);

            } else if (acao.equals("inicio")) {
                String url = "menu-gestor.jsp";
                response.sendRedirect(url);

            } else if (acao.equals("editar")) {
                this.editar(request, response);
                String url = "UsuarioControlador?acao=listar";
                response.sendRedirect(url);
            }

    }

    protected void inserir(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UsuarioDAO dao = new UsuarioDAO();
        Usuario a = new Usuario();
        Telefone telefone = new Telefone();
        a.setNome(request.getParameter("nome"));
        telefone.setNumero(request.getParameter("telefone"));
        List<Telefone> telefones = new ArrayList<Telefone>();
        telefones.add(telefone);
        a.setTelefones(telefones);
        a.setEmail(request.getParameter("email"));

        dao.inserir(a);
    }

    public void listar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UsuarioDAO dao = new UsuarioDAO();
        Collection<Usuario> lista = dao.listar();
        request.setAttribute("lista", lista);
    }

    protected void excluir(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String parametro = request.getParameter("id");
        int id = Integer.parseInt(parametro);
        UsuarioDAO dao = new UsuarioDAO();
        dao.excluir(id);
    }

    protected Usuario pesquisar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("matricula"));
        UsuarioDAO dao = new UsuarioDAO();
        Usuario a = new Usuario();
        a = dao.pesquisaUsuario(id);
        TelefoneDAO td = new TelefoneDAO();
        Collection<Telefone> lista = td.listar();
        request.setAttribute("telefones", lista);
        request.setAttribute("Usuario", a);
        for(Telefone telefone : a.getTelefones()){
        	request.setAttribute("telefone", telefone.getNumero());
            request.setAttribute("telefoneId", telefone.getIdTelefone());
        }
        request.setAttribute("acao", "iniciarEditar");
        return a;
    }

    protected void editar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UsuarioDAO dao = new UsuarioDAO();
        Usuario a = new Usuario();
        Telefone telefone = new Telefone();
        a.setIdUsuario(Integer.parseInt(request.getParameter("id")));
        a.setNome(request.getParameter("nome"));
        telefone.setNumero(request.getParameter("turma"));
        List<Telefone> telefones = new ArrayList<Telefone>();
        telefones.add(telefone);
        a.setTelefones(telefones);
        a.setEmail(request.getParameter("email"));

        dao.editar(a);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
