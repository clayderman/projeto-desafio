/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Telefone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import Usuario.Usuario;

/**
 *
 * @author clayderman soares
 */
@Entity
@Table(name="telefone")
public class Telefone {

    @Id
    @GeneratedValue
    @Column
    private int idTelefone;
    
    @ManyToOne
    private Usuario usuario;

    
    @Column
    private String numero;


	public int getIdTelefone() {
		return idTelefone;
	}


	public void setIdTelefone(int idTelefone) {
		this.idTelefone = idTelefone;
	}

	public String getNumero() {
		return numero;
	}


	public void setNumero(String numero) {
		this.numero = numero;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

    
}
