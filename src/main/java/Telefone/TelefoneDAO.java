/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Telefone;


import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Session;

import Usuario.Usuario;

/**
 *
 * @author clayderman soares
 */
public class TelefoneDAO {
    
	
	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("factory");
	
	private EntityManager em = factory.createEntityManager();
	

	
		public void inserir(Telefone d) {
	        
	    	em.getTransaction().begin();
	    	em.persist(d);
	    	em.getTransaction().commit();
	        em.close();
	        factory.close();
	    }
	
	
    public Collection<Telefone> listar(){
        Collection<Telefone> telefones = null;
        em.getTransaction().begin();
        telefones = em.createQuery("SELECT d FROM Telefone d").getResultList();
        em.getTransaction().commit();
        em.close();
        return telefones;
    }
}
