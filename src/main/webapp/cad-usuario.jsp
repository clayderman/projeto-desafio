<%-- 
    Document   : cad-usuario
    Created on : 22/05/2017, 17:14:27
    Author     : clayderman soares
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/cad-usuario.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="JS/jquery-3.2.0.js"></script>
<!--         <script type="text/javascript" src="JS/menu-gestor.js?nocache/>"></script> -->
		<script type="text/javascript" src="JS/menu-gestor.js?"></script>
        <title>Cadastrar Usuario</title>
        <script>
            var usuario = '${usuario}';
            var acao = '${acao}';
            
        </script>
    </head>
    <body>
        <div class="menu" style="margin-right: 500px; position: absolute;">
            <ul>
                <li class="current btnMenu"><a class="link" href="AlunoControlador?acao=inicio">Home</a></li>
                <li class="btnMenu"><a class="link" href="#">Usuarios</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu cadAluno" ><a class="link" href="AlunoControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="AlunoControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
            </ul>
        </div>
        <c:if test="${acao == 'carregaCadastro'}">
            <div class="formAluno">
                <form action="AlunoControlador?acao=inserir" method="post" style="margin-left: 250px; margin-top: 50px; margin-right: 250px;">
                    <div class="principalForm">
                        <div class="formulario">
                            <div class="legend">
                                <legend>Identificação</legend>
                            </div>
                            
                            
                            <p>Nome: 
                                <input type="text" id="nome" name="nome" required="required" placeholder="Nome" />
                            	<span id="erroNome" name="erros" style="color: red;"></span>    
                            </p>
                            
                            <p>Telefone: 
                                <input type="text" id="telefone" name="telefone" required="required" placeholder="Telefone" />
                            </p>
                        
                            <p>E-mail: 
                                <input type="text" id="email" required="required" name="email" placeholder="E-mail" />
                                <span id="erroEmail" name="erros" style="color: red;"></span>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <input id="btn" class="btnCadastrar" type="submit">                   
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
        <c:if test="${acao == 'iniciarEditar'}">
            <div class="formAluno">
                <form action="AlunoControlador?acao=editar" method="post" style="margin-left: 250px; margin-top: 50px; margin-right: 250px;">
                    <div class="principalForm">
                        <div class="formulario">
                            <div class="legend">
                                <legend>Identificação</legend>
                            </div>
                            <input type="hidden" name="id" value="${usuario.idUsuario}"/>
                            <p>Nome: 
                                <input type="text" name="nome" placeholder="Nome" value="${usuario.nome}" />
                            </p>
                            <p>Turma: 
                                <input type="text" name="telefone" placeholder="Telefone" value="${telefone.numero}" />
                            </p>
                        
                            <p>E-mail: 
                                <input type="text" name="email" placeholder="E-mail" value="${aluno.email}"/>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <input class="btnCadastrar btnEditar" type="submit" value="Editar">                   
                            <button class="btnCadastrar"><a href="AlunoControlador?acao=listar">Voltar para a consulta</a></button>               
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
    </body>
</html>
