<%-- 
    Document   : menu-gestor
    Created on : 31/03/2017, 16:04:43
    Author     : PC GAMER
--%>

<%@page import="Usuario.UsuarioDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Usuario.Usuario"%>
<%@page import="Usuario.UsuarioControlador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/lista-alunos.css?nocache" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="JS/jquery-3.2.0.js"></script>
        <script type="text/javascript" src="JS/menu-gestor.js?nocache/>"></script>
        <title>Menu</title>
        <script>
            function excluir(usuario) {
                $.ajax({
                    type: "POST",
                    url: "UsuarioControlador?acao=excluir&id=" + usuario,
                    success: function (result) {
                        location.reload();
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="menu" style="margin-right: 500px; position: absolute;">
            <ul>
                <li class="current btnMenu"><a class="link" href="AlunoControlador?acao=inicio">Home</a></li>
                <li class="btnMenu"><a class="link" href="#">Usuario</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu cadAluno" ><a class="link" href="AlunoControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="#">Consultar</a></li> 
                    </ul>
                </li>
            </ul>
        </div>
        <div class="tabela">
            <table class="tableAluno" style="position: absolute;">
                <thead>
                    <tr>
                        <th style="width: 80px;">ID</th>
                        <th style="width: 400px;">Nome</th>
                        <th style="width: 80px;">Telefone</th>
                        <th style="width: 80px;">Email</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="aluno" items="${lista}">
                        <tr>
                            <td><a href="AlunoControlador?acao=iniciarEditar&matricula=${usuario.idUsuario}"
                                   style="color: #fff; font: 100% open sans, sans-serif; font-size: 14px; border-width: thick;
                                   border-style: solid; border-color: #6f6f6f; border-radius: 7px 7px 7px 7px;
                                   -moz-border-radius: 7px 7px 7px 7px; -webkit-border-radius: 7px 7px 7px 7px; text-align: center;"
                                   >${usuario.idUsuario}</a></td>
                            <td>${usuario.nome}</td>
                            <td>${usuario.telefones.numero}</td>
                            <td style="width: 10px;" onclick="excluir(${usuario.idUsuario})">
                                <img src="imagens/icon_lixo.jpg">
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
