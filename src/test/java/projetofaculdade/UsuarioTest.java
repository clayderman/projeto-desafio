package projetofaculdade;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import Telefone.Telefone;
import Telefone.TelefoneDAO;
import Usuario.Usuario;
import Usuario.UsuarioDAO;
import junit.framework.TestCase;

public class UsuarioTest{

	@Test
	public void inserirusuario() {

		Usuario usuario = new Usuario();
		Usuario usuarioSetado = new Usuario();
		Telefone telefone = new Telefone();
		List<Telefone> telefones = new ArrayList<Telefone>();
		boolean resultEsperado = false;

		
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		TelefoneDAO telefoneDAO = new TelefoneDAO();
		
		telefone.setNumero("81996549969");
		telefones.add(telefone);
		
		usuario.setNome("Clayderman Augusto Soares");
		usuario.setTelefones(telefones);
		usuario.setEmail("teste321@gmail.com");
		
		usuarioSetado.setIdUsuario(usuario.getIdUsuario());
		
		usuarioDAO.inserir(usuario);
		
		
		UsuarioDAO dao = new UsuarioDAO();
        Usuario a = new Usuario();
		a = dao.pesquisaUsuario(usuarioSetado.getIdUsuario());
		
		if(a != null && !a.equals("")){
			resultEsperado = true;
		}
		
		Assert.assertTrue(resultEsperado);
		
	}
	
	@Test
	public void atualizarUsuario(){
	
		
		Usuario usuario = new Usuario();
		Usuario usuarioSetado = new Usuario();
		Telefone telefone = new Telefone();
		List<Telefone> telefones = new ArrayList<Telefone>();
		boolean resultEsperado = false;

		
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		
		
		usuario = usuarioDAO.pesquisaUsuario(1);
		
		usuario.setNome("Clayderman Augusto Oliveira Soares");
		TelefoneDAO telefoneDAO = new TelefoneDAO();
		
		telefone.setNumero("81988968732");
		telefones.add(telefone);
		usuario.setTelefones(telefones);
		usuario.setEmail("teste123@gmail.com");
		
		usuarioSetado.setIdUsuario(usuario.getIdUsuario());
		
		UsuarioDAO alDAO = new UsuarioDAO();
		
		alDAO.editar(usuario);
		
		
		UsuarioDAO dao = new UsuarioDAO();
        Usuario a = new Usuario();
		a = dao.pesquisaUsuario(usuarioSetado.getIdUsuario());
		
		if(a != null && !a.equals("")){
			resultEsperado = true;
		}
		
		Assert.assertTrue(resultEsperado);
		
		
		
	}
	
	
	@Test
	public void excluirusuario(){
		
		Usuario usuario = new Usuario();
			
		Usuario usuarioSetado = new Usuario();
		
		boolean resultEsperado = false;

		UsuarioDAO usuarioDAO = new UsuarioDAO();
		
		
		usuario = usuarioDAO.pesquisaUsuario(1);
		
		usuario.setIdUsuario(usuario.getIdUsuario());
		
		usuarioSetado = usuario;
		
		UsuarioDAO alDAO = new UsuarioDAO();
		
		alDAO.excluir(usuario.getIdUsuario());
		
		
		UsuarioDAO dao = new UsuarioDAO();
        Usuario a = new Usuario();
		resultEsperado = true;
		
		
		Assert.assertTrue(resultEsperado);
		
		
		
		
	}
	
	
	
	

}
