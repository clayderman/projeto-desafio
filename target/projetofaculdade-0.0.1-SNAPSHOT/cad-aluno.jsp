<%-- 
    Document   : cad-aluno
    Created on : 22/05/2017, 17:14:27
    Author     : PC GAMER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/cad-aluno.css?nocache" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="JS/jquery-3.2.0.js"></script>
        <script type="text/javascript" src="JS/menu-gestor.js?nocache/>"></script>
        <title>Cadastrar Aluno</title>
        <script>
            var aluno = '${aluno}';
            var acao = '${acao}';

        </script>
    </head>
    <body>
        <div class="menu" style="margin-right: 500px; position: absolute;">
            <ul>
                <li class="current btnMenu"><a class="link" href="AlunoControlador?acao=inicio">Home</a></li>
                <li class="btnMenu"><a class="link" href="#">Alunos</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu cadAluno" ><a class="link" href="AlunoControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="AlunoControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Professores</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu"><a class="link" href="ProfessorControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="ProfessorControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Turmas</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu"><a class="link" href="TurmaControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="TurmaControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Agenda</a></li>
            </ul>
        </div>
        <c:if test="${acao == 'carregaCadastro'}">
            <div class="formAluno">
                <form action="AlunoControlador?acao=inserir" method="post" style="margin-left: 250px; margin-top: 50px; margin-right: 250px;">
                    <div class="principalForm">
                        <div class="formulario">
                            <div class="legend">
                                <legend>Identificação</legend>
                            </div>
                            <p>Nome: 
                                <input type="text" name="nome" placeholder="Nome" />
                            </p>
                            <p>Turma: 
                                <select name="turma">
                                    <option>Selecione</option>
                                    <c:forEach var="turma" items="${turmas}">
                                        <option value="${turma.idturma}">${turma.descricao}</option>
                                    </c:forEach>
                                </select>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Endereço</legend>
                            </div>
                            <p>Rua: 
                                <input type="text" name="rua" placeholder="Rua" />
                            </p>
                            <p>Número: 
                                <input type="text" name="numero" placeholder="Número" />
                            </p>
                            <p>Bairro: 
                                <input type="text" name="bairro" placeholder="Bairro" />
                            </p>
                            <p>Cidade: 
                                <input type="text" name="cidade" placeholder="Cidade" />
                            </p>
                            <p>Estado: 
                                <input type="text" name="estado" placeholder="UF" />
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Identificação do responsável</legend>
                            </div>
                            <p>Nome: 
                                <input type="text" name="nomeResponsavel" placeholder="Nome" />
                            </p>
                            <p>RG: 
                                <input type="text" name="rg" placeholder="RG" />
                            </p>
                            <p>CPF: 
                                <input type="text" name="cpf" placeholder="CPF" />
                            </p>
                            <p>Telefone: 
                                <input type="text" name="telefone" placeholder="Telefone" />
                            </p>
                            <p>E-mail: 
                                <input type="text" name="email" placeholder="E-mail" />
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <input class="btnCadastrar" type="submit">                   
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
        <c:if test="${acao == 'iniciarEditar'}">
            <div class="formAluno">
                <form action="AlunoControlador?acao=editar" method="post" style="margin-left: 250px; margin-top: 50px; margin-right: 250px;">
                    <div class="principalForm">
                        <div class="formulario">
                            <div class="legend">
                                <legend>Identificação</legend>
                            </div>
                            <input type="hidden" name="id" value="${aluno.idaluno}"/>
                            <input type="hidden" name="matricula" value="${aluno.matricula}"/>
                            <p>Matricula: ${aluno.matricula}</p>
                            <p>Nome: 
                                <input type="text" name="nome" placeholder="Nome" value="${aluno.nome}" />
                            </p>
                            <p>Turma: 
                                <select name="turma">
                                    <option value="${turmaId}" selected>${turma}</option>
                                    <c:forEach var="valor" items="${turmas}">
                                        <option value="${valor.idturma}">${valor.descricao}</option>
                                    </c:forEach>
                                </select>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Endereço</legend>
                            </div>
                            <p>Rua: 
                                <input type="text" name="rua" placeholder="Rua" value="${aluno.rua}"/>
                            </p>
                            <p>Número: 
                                <input type="text" name="numero" placeholder="Número" value="${aluno.numero}"/>
                            </p>
                            <p>Bairro: 
                                <input type="text" name="bairro" placeholder="Bairro" value="${aluno.bairro}"/>
                            </p>
                            <p>Cidade: 
                                <input type="text" name="cidade" placeholder="Cidade" value="${aluno.cidade}"/>
                            </p>
                            <p>Estado: 
                                <input type="text" name="estado" placeholder="UF" value="${aluno.uf}"/>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Identificação do responsável</legend>
                            </div>
                            <p>Nome: 
                                <input type="text" name="nomeResponsavel" placeholder="Nome" value="${aluno.nomeResponsavel}"/>
                            </p>
                            <p>RG: 
                                <input type="text" name="rg" placeholder="RG" value="${aluno.rgResponsavel}"/>
                            </p>
                            <p>CPF: 
                                <input type="text" name="cpf" placeholder="CPF" value="${aluno.cpfResponsavel}"/>
                            </p>
                            <p>Telefone: 
                                <input type="text" name="telefone" placeholder="Telefone" value="${aluno.telefone}"/>
                            </p>
                            <p>E-mail: 
                                <input type="text" name="email" placeholder="E-mail" value="${aluno.email}"/>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <input class="btnCadastrar btnEditar" type="submit" value="Editar">                   
                            <button class="btnCadastrar"><a href="AlunoControlador?acao=listar">Voltar para a consulta</a></button>               
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
    </body>
</html>
