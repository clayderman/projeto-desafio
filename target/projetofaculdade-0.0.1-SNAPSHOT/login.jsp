<%-- 
    Document   : login
    Created on : 30/03/2017, 17:02:54
    Author     : PC GAMER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/login.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="JS/jquery-3.2.0.js"></script>
        <title>Login</title>
        <script>
            jQuery(document).ready(function () {
                mensagem();
            });
            
            function mensagem(){
                var msg = jQuery('#msg').val();
                if(msg){
                    alert(msg);
                }
            }

            function logar() {
                jQuery('#login').submit();
            }
        </script>
    </head>
    <body>
        <h1></h1>
        <h2></br></h2>
        <div class="nav">
            <form id="login" action="UsuarioControlador?acao=logar" method="post">
                <input type="text" name="matricula" class="username" placeholder="Matricula" />
                <input type="password" name="senha" class="password" placeholder="Senha" />
                <button class="button" onclick="logar();">Acessar</button></br>
                <label>
                    <input type="checkbox" value="Remember me" class="checkbox"/> <span>Lembrar registro</span>
                    <input type="hidden" id="msg" value="${mensagem}"/>
                </label>
            </form>

        </div>
    </body>
</html>
