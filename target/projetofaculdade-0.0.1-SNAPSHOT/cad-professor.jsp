<%-- 
    Document   : cad-aluno
    Created on : 22/05/2017, 17:14:27
    Author     : PC GAMER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/cad-aluno.css?nocache" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="JS/jquery-3.2.0.js"></script>
        <script type="text/javascript" src="JS/menu-gestor.js?nocache/>"></script>
        <title>Cadastrar Professor</title>
        <script>
            var aluno = '${aluno}';
            var acao = '${acao}';

            function cadastrar() {
                jQuery('input[name*="disciplina"]').each(function (index) {
                    if (jQuery(this).is(':checked') == false) {
                        jQuery(this).val(0);
                    }
                });

                jQuery('#cadProf').submit();
            }

            jQuery(document).ready(function () {
            <c:forEach var="disciplina" items="${disciplinasProf}">
                jQuery('input[name*="disciplina"]').each(function (index) {
                    if (jQuery(this).val() == ${disciplina.iddisciplina}) {
                        jQuery(this).attr("checked", true);
                    }
                });
            </c:forEach>
            });

        </script>
    </head>
    <body>
        <div class="menu" style="margin-right: 500px; position: absolute;">
            <ul>
                <li class="current btnMenu"><a class="link" href="AlunoControlador?acao=inicio">Home</a></li>
                <li class="btnMenu"><a class="link" href="#">Alunos</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu cadAluno" ><a class="link" href="AlunoControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="AlunoControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Professores</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu"><a class="link" href="ProfessorControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="ProfessorControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Turmas</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu"><a class="link" href="TurmaControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="TurmaControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Agenda</a></li>
            </ul>
        </div>
        <c:if test="${acao == 'carregaCadastro'}">
            <div class="formAluno">
                <form id="cadProf" action="ProfessorControlador?acao=inserir" method="post" style="margin-left: 250px; margin-top: 50px; margin-right: 250px;">
                    <div class="principalForm">
                        <div class="formulario">
                            <div class="legend">
                                <legend>Identificação</legend>
                            </div>
                            <p>Nome: 
                                <input type="text" name="nome" placeholder="Nome" />
                            </p>
                            <p>Salário 
                                <input type="text" name="salario" placeholder="Salário" />
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Disciplinas</legend>
                            </div>
                            <p>
                                <input type="hidden" id="count" name="count" value="${fn:length(disciplinas)}">
                                <c:forEach var="disciplina" items="${disciplinas}" varStatus="status">
                                    | ${disciplina.nome}
                                    <input type="checkbox" name="disciplina${status.count}" id="disciplina${disciplina.nome}" value="${disciplina.iddisciplina}">
                                </c:forEach>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Endereço</legend>
                            </div>
                            <p>Rua: 
                                <input type="text" name="rua" placeholder="Rua" />
                            </p>
                            <p>Número: 
                                <input type="text" name="numero" placeholder="Número" />
                            </p>
                            <p>Bairro: 
                                <input type="text" name="bairro" placeholder="Bairro" />
                            </p>
                            <p>Cidade: 
                                <input type="text" name="cidade" placeholder="Cidade" />
                            </p>
                            <p>Estado: 
                                <input type="text" name="estado" placeholder="UF" />
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Documentação / Contato</legend>
                            </div>
                            <p>RG: 
                                <input type="text" name="rg" placeholder="RG" />
                            </p>
                            <p>CPF: 
                                <input type="text" name="cpf" placeholder="CPF" />
                            </p>
                            <p>Telefone: 
                                <input type="text" name="telefone" placeholder="Telefone" />
                            </p>
                            <p>E-mail: 
                                <input type="text" name="email" placeholder="E-mail" />
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <button class="btnCadastrar" onclick="cadastrar();">Cadastrar</button>              
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
        <c:if test="${acao == 'iniciarEditar'}">
            <div class="formAluno">
                <form id="cadProf" action="ProfessorControlador?acao=editar" method="post" style="margin-left: 250px; margin-top: 50px; margin-right: 250px;">
                    <div class="principalForm">
                        <div class="formulario">
                            <div class="legend">
                                <legend>Identificação</legend>
                            </div>
                            <input type="hidden" name="id" value="${professor.idprofessor}"/>
                            <input type="hidden" name="matricula" value="${professor.matricula}"/>
                            <p>Matricula: ${professor.matricula}</p>
                            <p>Nome: 
                                <input type="text" name="nome" placeholder="Nome" value="${professor.nome}" />
                            </p>
                            <p>Salário: 
                                <input type="text" name="salario" placeholder="Salário" value="${professor.salario}" />
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Disciplinas</legend>
                            </div>
                            <p>
                                <input type="hidden" id="count" name="count" value="${fn:length(disciplinas)}">
                                <c:forEach var="disciplina" items="${disciplinas}" varStatus="status">
                                    | ${disciplina.nome}
                                    <input type="checkbox" name="disciplina${status.count}" id="disciplina${disciplina.nome}" value="${disciplina.iddisciplina}">
                                </c:forEach>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Endereço</legend>
                            </div>
                            <p>Rua: 
                                <input type="text" name="rua" placeholder="Rua" value="${professor.rua}"/>
                            </p>
                            <p>Número: 
                                <input type="text" name="numero" placeholder="Número" value="${professor.numero}"/>
                            </p>
                            <p>Bairro: 
                                <input type="text" name="bairro" placeholder="Bairro" value="${professor.bairro}"/>
                            </p>
                            <p>Cidade: 
                                <input type="text" name="cidade" placeholder="Cidade" value="${professor.cidade}"/>
                            </p>
                            <p>Estado: 
                                <input type="text" name="estado" placeholder="UF" value="${professor.uf}"/>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <div class="legend">
                                <legend>Documentação / Contato</legend>
                            </div>
                            <p>RG: 
                                <input type="text" name="rg" placeholder="RG" value="${professor.rg}"/>
                            </p>
                            <p>CPF: 
                                <input type="text" name="cpf" placeholder="CPF" value="${professor.cpf}"/>
                            </p>
                            <p>Telefone: 
                                <input type="text" name="telefone" placeholder="Telefone" value="${professor.telefone}"/>
                            </p>
                            <p>E-mail: 
                                <input type="text" name="email" placeholder="E-mail" value="${professor.email}"/>
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <button class="btnCadastrar btnEditar" onclick="cadastrar();">Editar</button>                   
                            <button class="btnCadastrar"><a href="ProfessorControlador?acao=listar">Voltar para a consulta</a></button>               
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
    </body>
</html>
