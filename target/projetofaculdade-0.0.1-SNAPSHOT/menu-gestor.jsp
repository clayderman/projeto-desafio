<%-- 
    Document   : menu-gestor
    Created on : 31/03/2017, 16:04:43
    Author     : PC GAMER
--%>

<%@page import="Aluno.AlunoDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Aluno.Aluno"%>
<%@page import="Aluno.AlunoControlador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/menu-gestor.css?nocache" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="JS/jquery-3.2.0.js"></script>
        <script type="text/javascript" src="JS/menu-gestor.js?nocache/>"></script>
        <title>Menu</title>
    </head>
    <body>
        <div class="menu" style="margin-right: 500px; position: absolute;">
            <ul>
                <li class="current btnMenu"><a href="#">Home</a></li>
                <li class="btnMenu"><a href="#">Alunos</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu cadAluno" ><a href="AlunoControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a href="AlunoControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a href="#">Professores</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu"><a href="ProfessorControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a href="ProfessorControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a href="#">Turmas</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu"><a href="TurmaControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a href="TurmaControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a href="#">Agenda</a></li>
            </ul>
        </div>
    </body>
</html>
