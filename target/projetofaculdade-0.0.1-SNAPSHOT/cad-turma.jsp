<%-- 
    Document   : cad-aluno
    Created on : 22/05/2017, 17:14:27
    Author     : PC GAMER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/cad-aluno.css?nocache" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="JS/jquery-3.2.0.js"></script>
        <script type="text/javascript" src="JS/menu-gestor.js?nocache/>"></script>
        <title>Cadastrar Turma</title>
        <script>
            var aluno = '${turma}';
            var acao = '${acao}';

        </script>
    </head>
    <body>
        <div class="menu" style="margin-right: 500px; position: absolute;">
            <ul>
                <li class="current btnMenu"><a class="link" href="AlunoControlador?acao=inicio">Home</a></li>
                <li class="btnMenu"><a class="link" href="#">Alunos</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu cadAluno" ><a class="link" href="AlunoControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="AlunoControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Professores</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu"><a class="link" href="#">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="#">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Turmas</a>
                    <ul class="subMenu" style="display: none;">
                        <li class="subMenu"><a class="link" href="TurmaControlador?acao=novoCadastro">Cadastrar</a></li> 
                        <li class="subMenu"><a class="link" href="TurmaControlador?acao=listar">Consultar</a></li> 
                    </ul>
                </li>
                <li class="btnMenu"><a class="link" href="#">Agenda</a></li>
            </ul>
        </div>
        <c:if test="${acao == 'carregaCadastro'}">
            <div class="formAluno">
                <form action="TurmaControlador?acao=inserir" method="post" style="margin-left: 250px; margin-top: 50px; margin-right: 250px;">
                    <div class="principalForm">
                        <div class="formulario">
                            <div class="legend">
                                <legend>Descrição</legend>
                            </div>
                            <p>Nome: 
                                <input type="text" name="descricao" placeholder="Descrição" />
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <input class="btnCadastrar" type="submit">                   
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
        <c:if test="${acao == 'iniciarEditar'}">
            <div class="formAluno">
                <form action="TurmaControlador?acao=editar" method="post" style="margin-left: 250px; margin-top: 50px; margin-right: 250px;">
                    <div class="principalForm">
                        <div class="formulario">
                            <div class="legend">
                                <legend>Descrição</legend>
                            </div>
                            <input type="hidden" name="id" value="${turma.idturma}"/>
                            <p>Descrição: 
                                <input type="text" name="descricao" placeholder="Descrição" value="${turma.descricao}" />
                            </p>
                        </div>
                        <div class="formulario" style="margin-top: 10px;">
                            <input class="btnCadastrar btnEditar" type="submit" value="Editar">                   
                            <button class="btnCadastrar"><a href="TurmaControlador?acao=listar">Voltar para a consulta</a></button>               
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
    </body>
</html>
